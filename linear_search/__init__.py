__version__ = '0.1.0'


def validate_ordering(order: str) -> ValueError:  # type: ignore
    """Raises ValueError on invalid order value

    Args:
        order: String must match a value of 'asc' or 'desc'
    Raises:
        ValueError: When order is not 'asc' or 'desc'
    """
    if order not in ["asc", "desc"]:
        raise ValueError("Sorting order must be 'asc', or 'desc'")


def sort_list(unsorted_list: list, order: str="asc") -> list:
    """Sort an unordered list into a desired sorting order

    Args:
        unsorted_list :: a list of unsorted values
        order :: string matching "asc" or "desc", defaults to "asc"
    Returns:
        A sorted list of the desired sorting order
    Raises:
        ValueError: If an invalid order is passed
    """
    # Raise ValueError if order arg is invalid
    validate_ordering(order)
    sorted_list = []
    while unsorted_list:
        min_value = unsorted_list[0]
        for number in unsorted_list:
            if number < min_value:
                min_value = number
        if order == "asc":
            sorted_list.append(min_value)
        elif order == "desc":
            sorted_list.insert(0, min_value)
        unsorted_list.remove(min_value)
    return sorted_list


def get_index_of_target(sorted_list: list, target: int) -> int:
    """Get index of target from list

    Args:
        sorted_list: list of elements
    Returns:
        Index value of the target within the list, if target does not exist, it
        will seek the next lowest value from the target.  If no value exists
        that's less than the target, returns -1.
    """
    target_index = -1
    while target_index == -1:
        try:
            target_index = sorted_list.index(target)
        except ValueError:
            if target < sorted_list[0]:
                break
            target -= 1
    return target_index


def position(arr: list, order: str, target: int) -> int: # type: ignore
    """Finds the index position of a target integer after sorting a list

    Sorts a list into a desired order and then seeks out the index value of the
    target.  Returns -1 when no target is found.

    Args:
        arr: Unsorted list array
        order: String value of 'asc' or 'desc'
        target: Integer to find the Index value from a sorted list
    Returns:
        Integer of the index position for the target within a sorted list
    """
    try:
        target_position = get_index_of_target(
            sort_list(arr, order),
            target
            )
        if target_position:
            return target_position
    except ValueError as err:
        print(err)
        raise

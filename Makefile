excludes = \*~ \*.pyc .cache/\* test_\* __pycache__/\*

.PHONY: run
run:
	poetry run pylint linear_search
	poetry run pytest
